package me.a8.callgc;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public final class CallGC extends JavaPlugin  {

    @Override
    public void onEnable() {
        int pluginId = 7194;
        Metrics metrics = new Metrics(this, pluginId);
        System.out.println(ChatColor.GREEN + "CallGC by a8_");
        getCommand("cgc").setExecutor(new cgc());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
